# wiki backend

Wiki Implementation based on a REST interface
Supports multiple identities and can communicate via p2p

Used core libraries:

* serde
* rocksdb
* tantivy
* libp2p (future)


## Description

* TBD

## Compile

```console
dev:~$ cargo build
```

## TODO

- [ ] Cleanup code structure
    - [ ] Try to split Application and Application Framework
    - [ ] Custom/Specific Errors
- [ ] Implement API handler
- [ ] Implement API logic
- [ ] Basic identity & repository logic
- [ ] Implement JWT Handling (WIP)
- [ ] Implement wiki logic
- [X] Move backend bin to separate folder
- [ ] Implement p2p sychronization
- [ ] Consider conflict-free replicated data type (CRDT) for wiki page storage & synchronization
