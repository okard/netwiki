
use  libp2p::mdns::service::*;


pub struct MdnsHandler
{
    // service id
    // channel -> events?
}

impl MdnsHandler
{
    pub fn new() -> Result<Self, anyhow::Error>
    {
        // peerid
        // listening ip addresses

        let mut service = MdnsService::new()?;
        Err(anyhow::anyhow!("not implemented"))
    }

    // inner future
}

/*
let mut service = MdnsService::new().expect("Error while creating mDNS service");
let _future_to_poll = futures::stream::poll_fn(move || -> Poll<Option<()>, io::Error> {
    loop {
        let packet = match service.poll() {
            Async::Ready(packet) => packet,
            Async::NotReady => return Ok(Async::NotReady),
        };

        match packet {
            MdnsPacket::Query(query) => {
                println!("Query from {:?}", query.remote_addr());
                query.respond(
                    my_peer_id.clone(),
                    my_listened_addrs.clone(),
                    Duration::from_secs(120),
                );
            }
            MdnsPacket::Response(response) => {
                for peer in response.discovered_peers() {
                    println!("Discovered peer {:?}", peer.id());
                    for addr in peer.addresses() {
                        println!("Address = {:?}", addr);
                    }
                }
            }
            MdnsPacket::ServiceDiscovery(query) => {
                query.respond(std::time::Duration::from_secs(120));
            }
        }
    }
}).for_each(|_| Ok(()));
*/
