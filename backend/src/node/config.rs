
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize)]
pub struct PeerConfig
{
    //key type?
    #[serde(with = "serde_bytes")]
    keypair : Vec<u8>,

    // listen address

    // known peers
    // trusted peers

    // private swarm settings
}

impl PeerConfig
{
    pub const STORAGEKEY: &'static str = "peer";

    pub fn new() -> PeerConfig
    {
        // generate key
        let keypair = libp2p::identity::ed25519::Keypair::generate();
        // convert it to binary
        let keypair = keypair.encode().to_vec();

        PeerConfig {
            keypair: keypair
        }
    }

    pub fn keypair(&mut self) -> &mut [u8] { &mut self.keypair }

    //return libp2p::identity::Keypair
}