
use std::sync::Arc;

use log::{info};

use libp2p::{
    Multiaddr,
    PeerId,
    Swarm,
    NetworkBehaviour,
    identity,
    mdns::{Mdns, MdnsEvent},
    swarm::NetworkBehaviourEventProcess
};

use crate::Backend;
use super::PeerConfig;


pub struct Peer
{
    // store peer id

    // internal states
    // discovered_peers
        // found but not yet trusted or untrusted
}

impl Peer
{
    pub fn new(mut config: PeerConfig) -> Result<Self, anyhow::Error> // PeerHandler trait as interface .e.g managing node trust list?
    {
        //keypair from config
        let key_data = &mut config.keypair();
        let keypair = libp2p::identity::ed25519::Keypair::decode(key_data)?;
        let keypair = libp2p::identity::Keypair::Ed25519(keypair);

        // create peer
        let peer_id = PeerId::from_public_key(keypair.public());
        info!("peer id: {:?}", peer_id);

        // transport
        let transport = libp2p::build_development_transport(keypair)?; // TODO own setup

        // behavior & swarm
        let mut swarm = 
        {
            let mdns = Mdns::new()?;
            let behaviour = P2pBehaviour {
                mdns
            };
            Swarm::new(transport, behaviour, peer_id)
        };

        // Listen on all interfaces and whatever port the OS assign
        Swarm::listen_on(&mut swarm, "/ip4/0.0.0.0/tcp/0".parse()?)?;

        // run swam future
        // TBD

        let peer = Peer {};

        //Err(err_msg("not implemented"))
        Ok(peer)
    }

    // access config to store current setup?

    // known nodes

}


#[derive(NetworkBehaviour)]
struct P2pBehaviour 
{
    mdns: Mdns,
    // todo own stuff similiar to floodsub with specialized protocol?
}

impl NetworkBehaviourEventProcess<MdnsEvent> for P2pBehaviour 
{
    // Called when `mdns` produces an event.
    fn inject_event(&mut self, event: MdnsEvent) 
    {
        match event 
        {
            MdnsEvent::Discovered(list) =>
                for (_peer, _) in list 
                {
                    //self.floodsub.add_node_to_partial_view(peer);
                }
            MdnsEvent::Expired(list) =>
                for (peer, _) in list 
                {
                    if !self.mdns.has_node(&peer) {
                        //self.floodsub.remove_node_from_partial_view(&peer);
                    }
                }
        }
    }
}


/*

use libp2p::{Multiaddr, Transport, tcp::TcpConfig};
let tcp = TcpConfig::new();
let addr: Multiaddr = "/ip4/98.97.96.95/tcp/20500".parse().expect("invalid multiaddr");
let _conn = tcp.dial(addr)


let keypair = libp2p::identity::Keypair::generate_ed25519();
let _transport = libp2p::build_development_transport(keypair);
// _transport.dial(...);

use libp2p::{Transport, tcp::TcpConfig, secio::SecioConfig, identity::Keypair};
let tcp = TcpConfig::new();
let secio_upgrade = SecioConfig::new(Keypair::generate_ed25519());
let tcp_secio = tcp.with_upgrade(secio_upgrade);
// let _ = tcp_secio.dial(...);


The easiest way to get started with libp2p involves the following steps:

    Creating an identity Keypair for the local node, obtaining the local PeerId from the PublicKey.
    Creating an instance of a base Transport, e.g. TcpConfig, upgrading it with all the desired protocols, such as for transport security and multiplexing. In order to be usable with a Swarm later, the Output of the final transport must be a tuple of a PeerId and a value whose type implements StreamMuxer (e.g. Yamux). The peer ID must be the identity of the remote peer of the established connection, which is usually obtained through a transport encryption protocol such as secio that authenticates the peer. See the implementation of build_development_transport for an example.
    Creating a struct that implements the NetworkBehaviour trait and combines all the desired network behaviours, implementing the event handlers as per the desired application's networking logic.
    Instantiating a Swarm with the transport, the network behaviour and the local peer ID from the previous steps.

The swarm instance can then be polled with the tokio library, in order to continuously drive the network activity of the program.

*/



// initialize p2p part
pub fn run_module_p2p(backend: Arc<Backend>) -> Result<Peer, anyhow::Error>
{
    let config: PeerConfig = {
        let store = backend.get_store();  
        store.get_or_init(PeerConfig::STORAGEKEY, || PeerConfig::new())?
    };

    let p = Peer::new(config)?;
    Ok(p)
}