
use std::path::Path;

use log::{info};

use tantivy::directory::MmapDirectory;
use tantivy::Index;
use tantivy::schema::*;
use tantivy::collector::TopDocs;
use tantivy::query::QueryParser;

use crate::logic::Page;
use crate::search::{SearchEngine, SearchResult};


pub struct TantivySearchEngine
{
    schema: Schema,
    index: Index
}

impl TantivySearchEngine
{
    //new build schema

    pub fn new<P: AsRef<Path>>(index_path: P) -> Result<Self, anyhow::Error>
    {
        let mut schema_builder = SchemaBuilder::default();
        schema_builder.add_text_field("title", TEXT | STORED);
        schema_builder.add_text_field("body", TEXT);
        // title, content, refkey, current_version?
        let schema = schema_builder.build();

        let directory = MmapDirectory::open(index_path)?;
        let index = Index::open_or_create(directory, schema.clone())
            .map_err(|err| anyhow::anyhow!("{}", err))?;

        let tse = TantivySearchEngine
        {
            schema,
            index
        };
        Ok(tse)
    }

    pub fn search_document(&self, query: &str) -> Result<SearchResult, anyhow::Error>
    {
        let title = self.schema.get_field("title")
            .ok_or_else(|| anyhow::anyhow!("expected title field to be available"))?;
        let body = self.schema.get_field("body")
            .ok_or_else(|| anyhow::anyhow!("expected body field to be available"))?;
        let query_parser = QueryParser::for_index(&self.index, vec![title, body]);

        let query = query_parser.parse_query(query)
            .map_err(|err| anyhow::anyhow!("error: {:?}", err))?;

        let reader = self.index.reader()
            .map_err(|err| anyhow::anyhow!("error: {}", err))?;
        let searcher = reader.searcher();
        let top_docs = searcher.search(&*query, &TopDocs::with_limit(10))
            .map_err(|err| anyhow::anyhow!("error: {}", err))?;

        let mut sr = SearchResult::default();
        for (_score, doc_address) in top_docs
        {
            let retrieved_doc = searcher.doc(doc_address).map_err(|err| anyhow::anyhow!("error: {}", err))?;
            let title_value = retrieved_doc.get_first(title)
                .ok_or_else(|| anyhow::anyhow!("expected title field is not available in document"))?;
            let title_value = title_value.text()
                .ok_or_else(|| anyhow::anyhow!("expected title field is not available in document"))?;

            sr.add_title(&title_value);
            //get title
            info!("{}", self.schema.to_json(&retrieved_doc));
        };

        Ok(sr)
    }

    pub fn index_page_inner(&mut self, page: &dyn Page)-> Result<(), anyhow::Error> // &PageContent
    {
        // ~ 5 MB heap memory
        let mut index_writer = self.index.writer(5_000_000)
            .map_err(|err| anyhow::anyhow!("error: {}", err))?;
        let title = self.schema.get_field("title")
            .ok_or_else(|| anyhow::anyhow!("expected title field to be available"))?;
        let body = self.schema.get_field("body")
            .ok_or_else(|| anyhow::anyhow!("expected body field to be available"))?;

        if page.mimetype().type_() != "text/markdown" {
            return Err(anyhow::anyhow!("can only index markdown"));
        }

        let content = std::str::from_utf8(&page.content())?;

        let mut page_document = Document::default();
        page_document.add_text(title, page.title());
        page_document.add_text(body, content);

        index_writer.add_document(page_document);
        index_writer.commit().map_err(|err|{ anyhow::anyhow!("error: {}", err) })?;

        Ok(())
    }

}

impl SearchEngine for TantivySearchEngine
{
    fn index_page(&mut self, page: &dyn Page) -> Result<(), anyhow::Error>
    {
        self.index_page_inner(page)?;
        Ok(())
    }

    fn search(&self, query: &str) -> Result<SearchResult, anyhow::Error>
    {
        let sr = self.search_document(query)?;
        Ok(sr)
    }
}
