
use crate::logic::Page;

use super::*;



// TODO define search result

pub trait SearchEngine
{
    fn index_page(&mut self, page: &dyn Page) -> Result<(), anyhow::Error>;

    fn search(&self, query: &str) -> Result<SearchResult, anyhow::Error>;

    //fn index_page(Page)
    //fn index_content(content)

    //fn search(query, options) -> SearchResult
}
