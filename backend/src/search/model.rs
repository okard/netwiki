

pub struct SearchQuery
{
    search_string: String
    // keywords / tags
    // max results
    // 
}

#[derive(Default)]
pub struct SearchResult
{
    titles: Vec<String>
}

impl SearchResult
{
    pub fn add_title(&mut self, title: &str)
    {
        self.titles.push(title.to_owned());
    }

    // iterate
}
