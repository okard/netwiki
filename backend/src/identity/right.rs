
use serde::{Serialize, Deserialize};


#[derive(Serialize, Deserialize, Debug)]
pub enum Right 
{
    // Node Wide Operations
    NodeRead,
    NodeList,
    IdentityList,

    // Identity Wide Operations    
}


//flags/bitset? 
//use std::collections::HashSet;
//HashSet<Right>
