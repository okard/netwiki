
use serde::{Serialize, Deserialize};

use rand::{thread_rng, Rng};

#[derive(Copy, Clone, Serialize, Deserialize, Debug, Default, Hash, Eq, PartialEq)]
pub struct IdentityId([u8; 32]);

impl IdentityId
{
    pub fn as_base58(&self) -> String
    {
        bs58::encode(self.0).into_string()
    }
}

#[derive(Serialize, Deserialize)]
struct EncryptedIdentityKeyPair
{
    password_hash: String,
    salt: [u8; 32],
    keypair_encrypted: Vec<u8>,
}

#[derive(Serialize, Deserialize)]
struct LoadedIdentity
{
    password_hash: String,
    salt: [u8; 32],
    keypair_decrypted: Vec<u8>,
}

enum IdentityState
{
    ///
    /// Unloaded Identity
    /// 
    Unloaded(EncryptedIdentityKeyPair),
    ///
    /// Loaded Identity
    /// 
    Active(LoadedIdentity)
}


// TODO identities are encrypted boxes and need to be unpacked on login

#[derive(Serialize, Deserialize)]
pub struct Identity
{
    id: IdentityId,
    name: String,
    // description
    // optional email address

    // internal_state
    // unencrypt keys

    password: String, // TODO Make safe / use it for key encryption
    salt: [u8; 32],
    // crypto keys // boxed and unboxed crypto keys?
    // id == hash of private crypto key ?
    // identity_id
    // user name
    // repos ids
    // rights/permissions
    // rights:

    //pw_change_requested: bool
}


impl Identity
{
    pub fn generate(name: &str, password: &str) -> Identity
    {
        let mut salt = [0u8; 32];
        thread_rng().fill(&mut salt[..]);

        // TODO generate keypair

        let identity = Identity {
            id: Identity::gen_id(),
            name: name.to_owned(),
            password: password.to_owned(), // TODO Argon2
            salt: salt
        };

        identity
    }

    // open() -> Result<(), Error> (decrypt user identity key)
    pub fn verify(&self, name: &str, password: &str) -> bool
    {
        //TODO load encrypted box
        self.name == name && self.password == password
    }

    pub fn id(&self) -> &IdentityId
    {
        &self.id
    }

    pub fn name(&self) -> &str
    {
        &self.name
    }

    fn gen_id() -> IdentityId
    {
        let mut bytes = [0u8; 32];
        thread_rng().fill(&mut bytes[..]);

        // convert to identity id
        use blake2::{Blake2b, Digest};
        let mut hasher = Blake2b::new();
        hasher.update(&bytes);
        let res = hasher.finalize();

        let mut identity_id : IdentityId = Default::default();
        identity_id.0.copy_from_slice(&res[0..32]);
        identity_id
    }

    // is_locked
    // unlock

    //key-value store
    
}

//open identity
