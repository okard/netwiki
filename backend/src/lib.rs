//#![deny(bare_trait_objects)]
#![feature(nll)]

//TODO libp2p network stuff to communicate with other wiki instances

mod storage; // storage for wiki data
mod search; // search system
mod logic; // bringing storage and search together
mod repository; // instances of wiki application
mod identity;   // identities/user of service
mod node;   // p2p network node

mod identityhandle; //active identity on backend (loaded repos)
mod store; //TODO better name basic object storage
mod backend; // the main backend

mod web; // the web api

// backend config
// TODO run backend and start network and webapi on top of it

// public api:
pub use crate::backend::{BackendConfig, Backend};
pub use crate::node::{run_module_p2p};
pub use crate::web::{run_module_webapi, WebApiConfig};
