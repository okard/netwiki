
use std::collections::HashMap;
use std::path::{PathBuf};
use std::sync::Arc;

use log::{warn, info};

use crate::store::{DataStore, FileStore};
use crate::identity::*;
use crate::identityhandle::*;

pub struct BackendConfig
{
    pub data_folder: PathBuf //builder pattern?
}

impl BackendConfig
{

}

pub struct Backend
{   
    // config reference
    config: BackendConfig,
    // identity state
    identities: HashMap<IdentityId, Arc<IdentityHandle>>,

    data_store: FileStore,
}

impl Backend
{
    // new backend
    pub fn new(config: BackendConfig) -> Result<Backend, anyhow::Error>
    {
        info!("initialized backend with folder '{}'", config.data_folder.display());
        
        let data_store = FileStore::new(&config.data_folder)?;

        let mut backend = Backend {
            config,
            identities: HashMap::new(),
            data_store: data_store,
        };
        backend.load_identities()?;

        Ok(backend)
    }

    // load the identities from file
    fn load_identities(&mut self) -> Result<(), anyhow::Error>
    {
        use std::fs::File;

        // load identity database
        let mut identities_db_path = self.config.data_folder.clone();
        identities_db_path.push("identities.cbor");

        // create default file if it does not exist
        if !identities_db_path.is_file() {
            // write
            let mut identities = Vec::new();
            identities.push(Identity::generate("admin", "password")); // default user //genmerate pw and write it to log?

            // make sure folder exists
            std::fs::create_dir_all(&self.config.data_folder)?;
            // serialize
            let mut file = File::create(&identities_db_path)?;
            serde_cbor::to_writer(&mut file, &identities)?;
        }
        // read the file
        let file = File::open(&identities_db_path)?;
        let identities : Vec<Identity> = serde_cbor::from_reader(&file)?;
        for identity in identities
        {
            let identity_handle = IdentityHandle::new(&self.config.data_folder, identity);
            self.identities.insert(identity_handle.identity().id().clone(), Arc::new(identity_handle));
        }
        Ok(())
    }

    // TODO better name ->  open/init/load/identity 
    pub fn verify_id(&self, name: &str, password: &str) -> Result<IdentityId, anyhow::Error>
    {
        // identity
        for (_, identity_handle) in &self.identities {
            if identity_handle.identity().name() == name
            &&  identity_handle.identity().verify(name, password ) {
                return Ok(identity_handle.identity().id().clone());
            }
        }
        Err(anyhow::anyhow!("identity not found"))
    }

    pub fn get_identity_handle(&self, id: &IdentityId) -> Result<Arc<IdentityHandle>, anyhow::Error>
    {
        self.identities.get(&id).map(|ih| ih.clone()).ok_or_else(|| anyhow::anyhow!("identity not available"))
    }

    // iterate Identities? lifetime iterator?
        //Iterator<Item = &Arc<IdentityHandle>>


    pub fn get_store(&self) -> &FileStore { &self.data_store }

    //access from 
}



