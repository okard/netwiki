

use serde::{Serialize, Deserialize};
use chrono::prelude::*;
use mime::{MediaType, APPLICATION_OCTET_STREAM};

use crate::logic::{Page, Content};

// Id/ Key
pub type RefKey = [u8; 32];  // consider RefKey([u8, 32])

// include meta data? like indexed timestamp

// page
#[derive(Serialize, Deserialize, Debug, Default)]
pub struct PageObject
{
    pub title: String,
    pub curr_main_content_ref: RefKey
}

// one version of content
#[derive(Serialize, Deserialize, Debug)]
pub struct ContentObject
{
    pub page_ref: RefKey, // owner
    pub prev_version_ref: Option<RefKey>,
    pub mimetype: MediaType,

    #[serde(with = "serde_bytes")]
    pub content: Vec<u8>,
    
    pub created: DateTime<Local>,
    // timestamp
}

impl Default for ContentObject
{
    fn default() -> Self
    {
        ContentObject{
            page_ref: RefKey::default(),
            prev_version_ref: None,
            mimetype: APPLICATION_OCTET_STREAM,
            content: Vec::new(),
            created: Local::now(),
        }
    }
}


// list of possible objects
#[derive(Serialize, Deserialize, Debug)]
pub enum Object
{
    Page(PageObject),
    Content(ContentObject)
}

impl Page for (PageObject, ContentObject) // move to logic with a struct wrapper
{
    fn title(&self) -> &str
    {
        &self.0.title
    }

    fn mimetype(&self) -> &MediaType
    {
        &self.1.mimetype
    }

    fn content(&self) -> &[u8]
    {
        &self.1.content
    }

    fn modified_at(&self) -> &DateTime<Local>
    {
        &self.1.created
    }
}

impl Content for ContentObject // move to logic
{
    fn mimetype(&self) -> &MediaType
    {
        &self.mimetype
    }

    fn content(&self) -> &[u8]
    {
        &self.content
    }
}
