
use thiserror::Error;

use crate::storage::{RefKey, Object};

// one wiki storage

#[derive(Error, Debug, PartialEq)]
pub enum StorageError
{
    #[error("Object does not exists")]
    NotExists,
    #[error("An unknown error has occurred.")]
    UnkownError
}

// bare minimum api storage for wiki objects
pub trait Storage
{
    fn get_object(&self, refkey: &RefKey) -> Result<Object, anyhow::Error>;
    //TODO only a put instead of create&update?
    //TODO refkey creation here?
    fn create_object(&mut self, refkey: &RefKey, obj: Object) -> Result<(), anyhow::Error>;
    fn update_object(&mut self, refkey: &RefKey, obj: Object) -> Result<(), anyhow::Error>;
    fn delete_object(&mut self, refkey: &RefKey) -> Result<(), anyhow::Error>;
}
