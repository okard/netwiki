
use std::path::Path;

use thiserror::Error;
use rocksdb::{self, DB, Options, DBCompressionType};
use bincode::{self, serialize, deserialize};

use crate::storage::{Storage, StorageError, RefKey, Object};

#[derive(Error, Debug)]
#[error("RocksDB Error")]
struct RocksDbError(rocksdb::Error); //TODO improve error message

#[derive(Error, Debug)]
#[error("Serialization Error")]
struct SerializationError(bincode::Error); //TODO improve error message


pub struct RocksDbStorage
{
    database: DB
}

impl RocksDbStorage
{
    pub fn new<P: AsRef<Path>>(path: P)  -> Result<Self, anyhow::Error>
    {
        let mut opts = Options::default();
        opts.create_if_missing(true);
        opts.set_compression_type(DBCompressionType::Lz4);

        let db = DB::open(&opts, path)
            .map_err(|err| anyhow::anyhow!("Error: {}", err))?;

        //return final db
        let rocks_db_storage = RocksDbStorage {
            database: db
        };
        Ok(rocks_db_storage)
    }
    // open & create?
}

// implement storage for rocksdb
impl Storage for RocksDbStorage
{
    fn get_object(&self, refkey: &RefKey) -> Result<Object, anyhow::Error>
    {
        let entry_opt = self.database.get(refkey)
            .map_err(|err| RocksDbError(err))?;
        let entry = entry_opt
            .ok_or_else(|| StorageError::NotExists)?;
        let obj : Object = deserialize(&entry).map_err(|err| SerializationError(err))?;
        Ok(obj)
    }

    fn create_object(&mut self, refkey: &RefKey, obj: Object) -> Result<(), anyhow::Error>
    {
        let value = serialize(&obj).map_err(|err| SerializationError(err))?;
        self.database.put(refkey, &value).map_err(|err| RocksDbError(err))?;
        Ok(())
    }

    fn update_object(&mut self, refkey: &RefKey, obj: Object) -> Result<(), anyhow::Error>
    {
        let value = serialize(&obj).map_err(|err| SerializationError(err))?;
        self.database.put(refkey, &value).map_err(|err| RocksDbError(err))?;
        Ok(())
    }

    fn delete_object(&mut self, refkey: &RefKey) -> Result<(), anyhow::Error>
    {
        self.database.delete(refkey).map_err(|err| RocksDbError(err))?;
        Ok(())
    }
}
