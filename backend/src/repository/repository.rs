


use std::path::Path;
use std::fs;
use std::sync::{Arc, RwLock};

use rand::{thread_rng, Rng};
use serde::{Serialize, Deserialize};

use crate::storage;
use crate::search;
use crate::logic::*;

#[derive(Serialize, Deserialize, Copy, Clone, Hash, Eq, PartialEq)]
pub struct RepositoryId([u8; 32]);

impl RepositoryId
{
    pub fn new() -> Self
    {
        let mut repo_id : RepositoryId = RepositoryId([0; 32]);
        thread_rng().fill(&mut repo_id.0[..]);
        repo_id
    }

    pub fn from_str(repository_id: &str) -> Result<Self, anyhow::Error>
    {
        let repo_id : RepositoryId = RepositoryId([0; 32]);
        bs58::decode(repository_id).into(repo_id.0)?;
        Ok(repo_id)
    }

    pub fn as_base58(&self) -> String
    {
        bs58::encode(self.0).into_string()
    }
}

#[derive(Serialize, Deserialize)]
struct RepositoryMetadata
{
    // the local repoid
    repository_id: RepositoryId,

    // for cloned repositories:
    orig_repository_id: Option<RepositoryId>,

    // repository_type: remote/local

    // application_type_uri : Uri, "application:wiki:v1"

    // owner_identity_id: IdentityId
    // name
    // description
    // folder?
    // size?

    // other identities trust db?
}

//repo manager?
    //open_repo(identy, repoid)

//trait for repository<App>?

pub struct Repository // make generic
{
    meta: RepositoryMetadata,
    //TODO keep storage and search here also?

    application: Arc<RwLock<dyn WikiApplication>> //high level api
    //WikiLogic
}

impl Repository
{
    //TODO config object -> metadata, repo_id, identity_id

    //TODO add network repository (network search, network storage)

    //TODO open options RepoId?

    // open or create?

    pub fn create<P: AsRef<Path>>(path: P) -> Result<Self, anyhow::Error>
    {
        //TODO check dir exists? // should be empty?
        let repo_id = RepositoryId::new();
        let mut path = path.as_ref().to_path_buf();
        path.push(repo_id.as_base58());
        let path = path;

        // Save repository meta data
        let meta = RepositoryMetadata {
            repository_id: repo_id,
            orig_repository_id : None,
        };
        fs::create_dir_all(&path)?;
        let mut metadata_path = path.clone();
        metadata_path.push("repository.cbor");

        let mut metadata_file = fs::File::create(&metadata_path)?;
        serde_cbor::to_writer(&mut metadata_file, &meta)?;

        //setup storage
        let mut storage_path = path.clone();
        storage_path.push("storage");
        let storage = storage::RocksDbStorage::new(&storage_path)
            .map_err(|err| anyhow::anyhow!("Error creating rocksdb storage {:?}", err))?;

        // setup search
        let mut search_path = path.clone();
        search_path.push("search");
        fs::create_dir_all(&search_path)?; //tantivy requires existing directory
        let search = search::TantivySearchEngine::new(&search_path)
            .map_err(|err| anyhow::anyhow!("Error creating tantivy {:?}", err))?;

        // setup logic
        let logic = WikiLogic::new(Box::new(storage), Box::new(search))
            .map_err(|err| anyhow::anyhow!("Error creating logic module {:?}", err))?;

        // prepare final repository
        let repo = Repository
        {
            meta,
            application: Arc::new(RwLock::new(logic))
        };

        Ok(repo)
    }

    pub fn open<P: AsRef<Path>>(path: P) -> Result<Self, anyhow::Error>
    {
        // read repository meta data
        let path = path.as_ref().to_path_buf();
        let mut metadata_path = path.clone();
        metadata_path.push("repository.cbor");

        let mut metadata_file = fs::File::open(&metadata_path)?;
        let meta : RepositoryMetadata = serde_cbor::from_reader(&mut metadata_file)?;

        //setup storage
        let mut storage_path = path.clone();
        storage_path.push("storage");
        let storage = storage::RocksDbStorage::new(&storage_path)
            .map_err(|err| anyhow::anyhow!("Error creating rocksdb storage {:?}", err))?;

        // setup search
        let mut search_path = path.clone();
        search_path.push("search");
        fs::create_dir_all(&search_path)?; //tantivy requires existing directory
        let search = search::TantivySearchEngine::new(&search_path)
            .map_err(|err| anyhow::anyhow!("Error creating tantivy {:?}", err))?;

        // setup logic
        let logic = WikiLogic::new(Box::new(storage), Box::new(search))
            .map_err(|err| anyhow::anyhow!("Error creating logic module {:?}", err))?;

        // prepare final repository
        let repo = Repository
        {
            meta,
            application: Arc::new(RwLock::new(logic))
        };
        Ok(repo)
    }

    // clone_remote

    pub fn id(&self) -> &RepositoryId
    {
        &self.meta.repository_id
    }

    //get_wiki_instance
    pub fn get_application(&self) -> &RwLock<dyn WikiApplication>
    {
        &*self.application
    }
}


// implement a remote repository without local storage
    // Trait Repository for RemoteRepository
    