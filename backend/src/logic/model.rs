

pub use mime::MediaType;
use chrono::prelude::*;

//Page

// different content types?

//TODO trait & struct system?
//TODO better name
pub trait Page
{
    fn title(&self) -> &str;
    fn mimetype(&self) -> &MediaType;
    fn content(&self) -> &[u8];
    // tags?
    // creator -> Identity id?
    // version?

    // created date
    fn modified_at(&self) -> &DateTime<Local>;

    //sidebar page?
}

pub trait Content
{
    fn mimetype(&self) -> &MediaType;
    fn content(&self) -> &[u8];
}

// based on a index and content version?


// Page history

// Page resources
