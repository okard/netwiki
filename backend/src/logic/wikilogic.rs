
//search trait


use crate::storage::{RefKey, Object, PageObject, ContentObject};
use crate::storage::{Storage, StorageError};
use crate::search::{SearchEngine};
use crate::logic::{WikiError, WikiApplication, Page, Content};

// implement the high level api based on storage & search traits
pub struct WikiLogic
{
    // storage id [u8; 32]
    storage: Box<dyn Storage>,
    search_engine: Box<dyn SearchEngine>
    //TODO options: how many versions to keep for page etc
}


impl WikiLogic
{
    //TODO add additional config
    pub fn new(storage: Box<dyn Storage>, search_engine: Box<dyn SearchEngine>) -> Result<Self, ()>
    {

        let wiki_logic = WikiLogic{
            storage,
            search_engine
        };
        Ok(wiki_logic)
    }


    //title to RefKey

}


unsafe impl Send for WikiLogic {} // TODO verify
unsafe impl Sync for WikiLogic {} // TODO verify

impl WikiApplication for WikiLogic
{
    fn get_page(&self, title: &str) -> Result<Box<dyn Page>, anyhow::Error>
    {
        let ref_key = get_ref_key(title.as_bytes());

        // receive object from object storage
        let obj = self.storage.get_object(&ref_key)?;

        let po : Result<PageObject, anyhow::Error> = match obj {
            Object::Page(p) => Ok(p),
            _ => Err(WikiError::UnkownError.into())
        };
        let po = po?;

        let obj = self.storage.get_object(&po.curr_main_content_ref)?;
        let poc : Result<ContentObject, anyhow::Error> = match obj {
            Object::Content(poc) => Ok(poc),
            _ => Err(WikiError::UnkownError.into())
        };
        let poc = poc?;

        Ok(Box::new((po, poc)))
    }

    fn save_page(&mut self, pagec: &dyn Page) -> Result<(), anyhow::Error>
    {
        // calculate key from title
        let page_refkey = get_ref_key(&pagec.title().as_bytes());

        //create page when not exists
        let page_obj = self.storage.get_object(&page_refkey);
        let page : Result<PageObject, anyhow::Error> = match page_obj {
            Ok(Object::Page(p)) => Ok(p),
            Err(ref err) if err.downcast_ref::<StorageError>().map_or(false, |t| *t == StorageError::NotExists) => Ok(PageObject::default()),
            Err(err) => Err(anyhow::anyhow!("Storage error {:?}", err)),
            _ => Err(anyhow::anyhow!("Object mismatch"))
        };
        let mut page = page?;

        // prepare content
        let mut content = ContentObject::default();
        content.page_ref = page_refkey;
        content.prev_version_ref = Some(page.curr_main_content_ref);
        content.mimetype = pagec.mimetype().clone();
        content.content = pagec.content().to_vec();
        let content_refkey = get_ref_key(&content.content);

        // save content in storage
        self.storage.update_object(&content_refkey, Object::Content(content))?;

        // update the page reference
        page.title = pagec.title().to_owned();
        page.curr_main_content_ref = content_refkey;

        self.storage.update_object(&page_refkey, Object::Page(page))?;

        // update search index
        self.search_engine.index_page(pagec)?;




        Err(WikiError::UnkownError.into())
    }


    fn get_content(&self, ref_key: &RefKey) -> Result<Box<dyn Content>, anyhow::Error>
    {
        let obj = self.storage.get_object(ref_key)?;

        let co : Result<ContentObject, anyhow::Error> = match obj {
            Object::Content(c) => Ok(c),
            _ => Err(WikiError::UnkownError.into())
        };
        let co = co?;
        Ok(Box::new(co))
    }
}

//get the page refkey key for title
fn get_ref_key(content : &[u8]) -> RefKey
{
    // convert title to ref key
    use blake2::{Blake2b, Blake2s, Digest};
    let mut hasher = Blake2b::new();
    hasher.update(&content);
    let res = hasher.finalize();

    let mut ref_key : RefKey = Default::default();
    ref_key.copy_from_slice(&res[0..32]);
    ref_key
}
