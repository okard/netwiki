
use thiserror::Error;

use crate::storage::{RefKey};
use super::{Page, Content};

#[derive(Error, Debug)]
pub enum WikiError
{
    #[error("An unknown error has occurred.")]
    UnkownError
}


// high level api //TODO better name (WikiApplication)
pub trait WikiApplication : Send + Sync
{
    // query/search

    // get single page
    fn get_page(&self, title: &str) -> Result<Box<dyn Page>, anyhow::Error>;

    // page with main contents (separate logic model)

    // page resource management (images, etc)

    fn save_page(&mut self, page: &dyn Page) -> Result<(), anyhow::Error>;

    // save

    // get resource for page?
    fn get_content(&self, key: &RefKey) -> Result<Box<dyn Content>, anyhow::Error>;

    // get_page_history (with paging)

    // clean up
}
