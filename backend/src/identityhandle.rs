use std::path::{Path, PathBuf};
use std::collections::HashMap;
use std::sync::RwLock;

use log::{warn, info};

use crate::identity::*;
use crate::repository::*;

pub struct IdentityHandle //TODO better name IdentityContext?
{
    identity: Identity,
    identity_folder: PathBuf,
    // path to identity folder
    repositories: RwLock<HashMap<RepositoryId, Repository>>
}

impl IdentityHandle
{
    //new with identity + folder
    pub fn new(base_path: &Path, identity: Identity) -> Self
    {
        let folder = {
            let mut folder = base_path.to_path_buf();
            folder.push(&identity.id().as_base58());
            folder
        };

        // convert to handle
        let identity_handle = IdentityHandle {
            identity,
            identity_folder: folder,
            repositories : RwLock::new(HashMap::new())
        };
        identity_handle.load_repos();
        identity_handle
    }
    // add_repo
    // request_repo

    pub fn identity(&self) -> &Identity
    {
        &self.identity
    }

    fn load_repos(&self)
    {
        // open folder
        let dir_iterator = std::fs::read_dir(&self.identity_folder);
        let dir_iterator = match dir_iterator
        {
            Err(err) => {
                warn!("Can't iterate repo folder of id {} because of {}", self.identity.id().as_base58(), err);
                return;
            },
            Ok(iter) => iter
        };

        // try to open repositories
        for entry in dir_iterator
        {
            let entry = match entry {
                Ok(e) => e,
                Err(_) => continue
            };
            let path = entry.path();
            if path.is_dir()
            {
                match Repository::open(&path)
                {
                    Ok(repository) => {
                        info!("open repo {}", repository.id().as_base58());
                        let mut repositories = self.repositories.write().unwrap();
                        repositories.insert(repository.id().clone(), repository);
                    },
                    Err(err) => warn!("can't open repository '{:?}' '{}'", path, err)
                }

            }
        }

    }

    pub fn list_repository(&self) -> Vec<RepositoryId>
    {
        let repositories = self.repositories.read().unwrap();
        repositories.keys().map(|rid| rid.clone()).collect()
    }

    pub fn create_repository(&self) -> Result<RepositoryId, anyhow::Error>
    {
        let repository = Repository::create(&self.identity_folder)?;
        let id = repository.id().clone();

        let mut repositories = self.repositories.write().unwrap();
        repositories.insert(repository.id().clone(), repository);

        Ok(id)
    }

    pub fn get_repository(&self, repository_id: &RepositoryId) -> Result<&Repository, anyhow::Error>
    {
        Err(anyhow::anyhow!("not yet implemented"))
    }

    // get access to repository
}
