

use serde::{Serialize, Deserialize};
use mime::MediaType;
use chrono::prelude::*;

use crate::logic::Page;
use crate::identity::IdentityId;

// json models

// LOGIN ====================================================

#[derive(Serialize, Deserialize, Debug)]
pub struct TokenPayload
{
    pub user: String,
    pub identity_id: IdentityId,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct TokenRequest
{
    pub user: String,
    pub password: String //already hashed?
}

#[derive(Serialize, Deserialize, Debug)]
pub struct TokenResponse
{
    pub access_token: String
}

// Identity ====================================================

#[derive(Serialize, Deserialize, Debug)]
pub struct PasswordChangeRequest
{
    pub old_password: String,
    pub new_password: String,
}

// update profile


// REPOSITORY ====================================================

#[derive(Serialize, Deserialize, Debug)]
pub struct Repository
{
    pub repository_id: String,
}

pub type RepositoryList = Vec<Repository>;


#[derive(Serialize, Deserialize, Debug)]
pub struct RepositoryCreateRequest
{
    pub name: String,
    //options: private/public
}

// PAGE ====================================================

/// Page Response //move to logic
#[derive(Serialize, Deserialize, Debug)]
pub struct PageResponse
{
    pub title: String,
    pub current_content: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct PageCreateRequest
{
    pub title: String,
    pub current_content: String,
    created: DateTime<Local>, // serde default
    mime_type: MediaType, // serde default
}

impl Page for PageCreateRequest
{
    fn title(&self) -> &str
    {
        &self.title
    }
    fn mimetype(&self) -> &MediaType
    {
        &self.mime_type
    }

    fn content(&self) -> &[u8]
    {
        self.current_content.as_bytes()
    }

    fn modified_at(&self) -> &DateTime<Local>
    {
        &self.created
    }
}


// PageCreateUpdateRequest


// SEARCH ====================================================

// TODO SearchRequest
// TODO SearchResultResponse


// NODE ====================================================

// TODO Node(Peer)List
// TODO Connected Peers
// TODO Manage Node Trusted Peers
// TODO Manage Node Untrusted Peers
// TODO Manage Node Undefinied Peers
// TODO Peers Identities & Repos