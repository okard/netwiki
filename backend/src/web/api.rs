
//use std::error::Error;
use std::sync::Arc;
use std::time::{Duration};

use log::{info};

//use actix_web::{error, http};
use actix_rt::{System};
use actix_web::*;
use actix_cors::Cors;

use bs58;

use crate::identity::*;
use crate::repository::{RepositoryId};

use super::*;
use super::model::*;
use super::token::AccessToken;

// create a jwt token to be authenticated for future actions
async fn create_token(req: HttpRequest) -> Result<HttpResponse>
{
    info!("create_token");

    let data = req.app_data::<Arc<WebApiState>>()
        .ok_or_else(|| HttpResponse::InternalServerError().body("failed to access state") )?;

    let token_request = web::Json::<TokenRequest>::extract(&req).await
        .map_err(|err| HttpResponse::BadRequest().body(format!("{}", err)) )?;

    let secret = data.get_secret();
    //open identity
    // token api
        // identity provider?
        // rights of user for requested storage

    // TODO Support different authentification methods
    // TODO refresh token? separate route?

    //TODO Check identity
    // get identity
    let identity = data.backend().verify_id(&token_request.user, &token_request.password)
        .map_err(|_| error::ErrorUnauthorized("Login failed"))?;

    // payload of the token
    let payload = TokenPayload
    {
        user: token_request.user.clone(),
        identity_id: identity
    };

    let access_token = AccessToken::new(payload, Duration::from_secs(60*60*5)) // TODO lifetime from config
        .map_err(|err| error::ErrorInternalServerError(format!("Can't create token {}", err)))?;

    // create web api result
    let tr = TokenResponse
    {
        access_token: access_token.to_token(secret)
            .map_err(|err| error::ErrorInternalServerError(format!("Can't create token {}", err)))?
    };

    Ok(HttpResponse::Created().json(tr))  // <- send response
}

// get list of repositories
async fn get_repositories(req: HttpRequest) -> Result<HttpResponse>
{
    info!("get_repositories");

    let data = req.app_data::<Arc<WebApiState>>()
        .ok_or_else(|| HttpResponse::InternalServerError().finish() )?;

    let identity_id = check_auth(&req)?;
    let id_handle = data.backend().get_identity_handle(&identity_id)
        .map_err(to_response_error)?;

    let repos : Vec<Repository> = id_handle.list_repository().iter()
        .map(|rid| Repository { repository_id: rid.as_base58() }).collect();

    Ok(HttpResponse::Ok().json(repos))
}

async fn create_repository(req: HttpRequest) -> Result<HttpResponse>
{
    info!("create_repository");

    let data = req.app_data::<Arc<WebApiState>>()
        .ok_or_else(|| HttpResponse::InternalServerError().finish() )?;

    let identity_id = check_auth(&req)?;

    let id_handle = data.backend().get_identity_handle(&identity_id)
        .map_err(to_response_error)?;

    let repository_id = id_handle.create_repository()
        .map_err(to_response_error)?;

    let response = HttpResponse::Created()
            .header(http::header::CONTENT_TYPE, "application/json")
            .header(http::header::LOCATION, format!("/repositories/{}", repository_id.as_base58()))
            .finish();
    Ok(response)
}

// get one repositories
async fn get_repository(req: HttpRequest) -> Result<HttpResponse>
{
    info!("get_repository");
    let _identity = check_auth(&req)?;

    // repository info
    let _list : RepositoryList =Vec::new();

    Ok(HttpResponse::NotImplemented().finish())
}

// create a new wiki page
async fn create_page(req: HttpRequest) -> Result<HttpResponse>
{
    info!("create_page");

    let data = req.app_data::<Arc<WebApiState>>()
        .ok_or_else(|| HttpResponse::InternalServerError().finish() )?;

    let repository_id: &str = req.match_info().query("repositoryId");
    let repository_id = RepositoryId::from_str(repository_id)
        .map_err(to_response_error)?;

    let identity_id = check_auth(&req)?;

    let id_handle = data.backend().get_identity_handle(&identity_id)
        .map_err(to_response_error)?;

    // get repo for id
    let repo = id_handle.get_repository(&repository_id)
        .map_err(to_response_error)?;


    let page_create_request = web::Json::<PageCreateRequest>::extract(&req)
        .await
        .map_err(|err| HttpResponse::BadRequest().body(format!("{}", err)) )?
        .into_inner();

    {
        let mut app = repo.get_application().write()
            .map_err(|err| HttpResponse::InternalServerError().body(format!("{}", err)))?;
        app.save_page(&page_create_request)
            .map_err(to_response_error)?;
    }

    Ok(HttpResponse::NotImplemented().finish())
}

// create a new wiki page
async fn update_page(req: HttpRequest) -> Result<HttpResponse>
{
    info!("update_page");
    let _identity = check_auth(&req)?;
    // check jwt
    // bs58::decode("he11owor1d").into_vec()

    // get identity from state
    // get repo from state

    let name : &str = req.match_info().query("name");
    Ok(HttpResponse::Ok().body(format!("Hello {}!", name)))
}

// get a wiki page
async fn get_page(req: HttpRequest) -> Result<HttpResponse>
{
    info!("get_page");
    let _identity = check_auth(&req)?;

    //TODO identity via jwt
    let repository_id: &str = req.match_info().query("repositoryId");
    let title: &str = req.match_info().query("title");

    let _repository_id = bs58::decode(&repository_id).into_vec()
        .map_err(|e| error::ErrorBadRequest(e.to_string().to_owned()))?;
    //let repo = req.state().get_repository(identity_id, &repository_id).map_err(|_| error::ErrorBadRequest("no repo"))?;

    let pr = PageResponse {
        title: title.to_owned(),
        current_content: "test".to_owned()
    };

    Ok(HttpResponse::Ok().json(pr))
}

// get one repositories
async fn get_page_history(_req: HttpRequest) -> impl Responder
{
    info!("get_page_history");
    HttpResponse::NotImplemented()
}

async fn get_page_version(_req: HttpRequest) -> impl Responder
{
    info!("get_page_version");
    HttpResponse::NotImplemented()
}

// get object?

// search


// wiki api
    // <storage>/page/<title-string>
    // <storage>/obj/<obj-id>

    // new page push?

    // <storage>/obj/<obj-id>/history

    // search

// p2p-node api

fn check_auth(req: &HttpRequest) -> Result<IdentityId>
{
    let data = req.app_data::<Arc<WebApiState>>()
        .ok_or_else(|| HttpResponse::InternalServerError().finish() )?;

    // read auth header
    let header = req.headers().get(http::header::AUTHORIZATION)
        .ok_or(error::ErrorBadRequest("Bad Request"))?;

    let auth_value = header.to_str()
        .map_err(|e| error::ErrorBadRequest(format!("{}", e)))?;

    // Check format: "Bearer token.hmac"
    if !auth_value.starts_with("Bearer ")
    {
        return Err(error::ErrorBadRequest("Not a bearer token"));
    }

    let token = &auth_value["Bearer ".len()..];

    let secret = data.get_secret();
    let access_token = AccessToken::<TokenPayload>::from_token(token, secret)
        .map_err(|e| error::ErrorUnauthorized(format!("{}", e)))?;

    Ok(access_token.payload().identity_id)
}

async fn spec(_req: HttpRequest) -> impl Responder {
    let spec  = include_str!("openapi.yml");

    HttpResponse::Ok()
        .content_type("text/yaml")
        .body(spec)
}


use crate::{ backend::Backend};
pub fn run_module_webapi(backend: Arc<Backend>) -> Result<(), anyhow::Error>
{
    
    //TODO put in config
    //TODO consider graphql

    let config: WebApiConfig = {
        let store = backend.get_store();
        store.get_or_init(WebApiConfig::STORAGEKEY, || WebApiConfig::default())?
    };

    info!("starting web server with rest api on: {}", config.get_socket_address());
    info!("using secret {:x?}", config.local_secret().iter().map(|x| format!("{:x}", x)).collect::<Vec<String>>().join(""));

    let state = Arc::new(WebApiState::new(backend, &config));

    let sys = System::new("netwiki_web_api");

    HttpServer::new(move || {

        let cors = Cors::new()
        //.allowed_origin("All")
        .allowed_methods(vec!["GET", "POST", "PUT", "DELETE"])
        //.allowed_headers(vec![http::header::AUTHORIZATION, http::header::ACCEPT])
        //.allowed_header(http::header::CONTENT_TYPE)
        .max_age(3600)
        .finish();

        App::new()
            .data(state.clone())
            .service(
             web::scope("/wiki")
            .wrap(cors)
            .route("/openapi.yml", web::get().to(spec))
            .route("/token", web::post().to(create_token))
            .route("/repositories", web::get().to(get_repositories))
            .route("/repositories", web::post().to(create_repository))
            .route("/repositories/{repositoryId}", web::get().to(get_repository))
            .route("/repositories/{repositoryId}/pages/{title}", web::get().to(get_page))
            .route("/repositories/{repositoryId}/pages/{title}", web::post().to(create_page))
            .route("/repositories/{repositoryId}/pages/{title}", web::put().to(update_page))
            .route("/repositories/{repositoryId}/pages/{title}/history", web::get().to(get_page_history))
            .route("/repositories/{repositoryId}/pages/{title}/{versionid}", web::get().to(get_page_version))
            // /network/nodes POST, GET
            // /network/nodes/{id} GET, PUT, DELETE
            // /identities/ POST, GET
            // /identities/{id} GET, PUT, DELETE
            // /identities/my GET
            )
    })
    .bind(config.get_socket_address())
    .unwrap()
    .run();

    sys.run()?;

    Ok(())
}


fn to_response_error(error: anyhow::Error)
{
    actix_web::error::InternalError::new(error, actix_web::http::StatusCode::INTERNAL_SERVER_ERROR);
}