
use std::sync::{Arc};

//use log::{info, error};

use crate::{backend::{Backend}};
use super::WebApiConfig;

//Config

pub struct WebApiState
{
    //token provider && token checker
    //secret
    local_secret: [u8; 16], //for JWT generation
    backend: Arc<Backend>,
}

impl WebApiState
{
    pub fn new(backend: Arc<Backend>, config: &WebApiConfig) -> Self //input backend
    {
        // setup web state
        let state = WebApiState {
            local_secret: config.local_secret().clone(),
            backend
        };

        state
    }

    pub fn get_secret(&self) -> &[u8]
    {
        &self.local_secret
    }

    pub fn backend(&self) -> &Arc<Backend>
    {
        &self.backend
    }

}
