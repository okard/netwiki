

use std::time::{Duration, SystemTime};

use thiserror::Error;

use serde::{Serialize, Deserialize};
use serde::de::DeserializeOwned;

use sha2::Sha256;
use hmac::{Hmac, Mac, NewMac};
type HmacSha256 = Hmac<Sha256>;

use serde_json;
use base64;


#[derive(Error, Debug, PartialEq)]
pub enum AccessTokenError
{
    #[error("Access token handling failed because of {0}")]
    StaticMessage(&'static str),
    #[error("Access token handling failed because of {0}")]
    Message(String),
}


#[derive(Serialize, Deserialize, Debug)]
pub struct AccessToken<T: Serialize>
{
    expiry: u64,
    payload: T
}

impl<T> AccessToken<T>
    where T: serde::Serialize, T: DeserializeOwned
{
    pub fn new(payload: T, duration: Duration) -> Result<AccessToken<T>, anyhow::Error>
    {
        use std::ops::Add;
        let duration = SystemTime::now()
                        .add(duration)
                        .duration_since(SystemTime::UNIX_EPOCH);
        let duration = duration?;

        Ok(AccessToken {
            expiry: duration.as_secs(),
            payload
        })
    }

    pub fn to_token(&self, secret: &[u8]) -> Result<String, anyhow::Error>
    {
        let access_token = serde_json::to_string(&self)?;
        let mut mac = HmacSha256::new_varkey(secret)
            .map_err(|_err| AccessTokenError::StaticMessage("HMAC: Invalid key length"))?;
        mac.update(&access_token.as_bytes());
        let result = mac.finalize();

        let token = format!("{}.{}", base64::encode(&access_token), base64::encode(&result.into_bytes()));

        Ok(token)
    }

    pub fn from_token(token: &str, secret: &[u8]) -> Result<AccessToken<T>, anyhow::Error>
    {
        let token_parts: Vec<&str> = token.split('.').collect();

        if token_parts.len() != 2 {
            return Err(AccessTokenError::StaticMessage("Token has not the right format").into());
        }

        // unpack token
        let content = base64::decode(token_parts[0])?;
        let hmac = base64::decode(token_parts[1])?;

        // check signature
        let mut mac = HmacSha256::new_varkey(secret)
            .map_err(|_err| AccessTokenError::StaticMessage("HMAC: Invalid key length"))?;
        mac.update(&content);
        mac.verify(&hmac)
            .map_err(|err| AccessTokenError::Message(format!("{}", err)))?;

        // convert token and return id
        let access_token : AccessToken<T> = serde_json::from_slice(&content)?;

        // check expiry
        let duration = SystemTime::now()
                        .duration_since(SystemTime::UNIX_EPOCH);
        let duration = duration?;

        if access_token.expiry < duration.as_secs() {
            return Err(AccessTokenError::StaticMessage("The token has been expired").into());
        }

        Ok(access_token)
    }

    pub fn payload(&self) -> &T
    {
        &self.payload
    }

}

// TODO: tests
