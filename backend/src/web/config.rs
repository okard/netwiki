

//implement trait with a default struct?

use std::net::{SocketAddr};
use std::str::{FromStr};

use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct WebApiConfig //Serialize
{
    socket_address: SocketAddr, //TODO vec to bind multiple addresses
    // socket address binding
    local_secret: [u8; 16], // for jwt tokens
    //worker count
    
}

impl WebApiConfig
{
    pub const STORAGEKEY: &'static str = "webapiconfig";

    pub fn get_socket_address(&self) -> SocketAddr
    {
        self.socket_address
    }

    pub fn local_secret(&self) -> &[u8; 16] { &self.local_secret }
}

impl Default for WebApiConfig
{
    fn default() -> Self
    {
        // create secret for jwt tokens
        let mut secret = [0u8; 16];
        use rand::{thread_rng, Rng};
        thread_rng().fill(&mut secret);

        // finalize config
        WebApiConfig
        {
            socket_address: SocketAddr::from_str("127.0.0.1:8085").unwrap(),
            local_secret: secret
        }
    }
}
