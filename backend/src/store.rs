use serde::{Serialize, de::DeserializeOwned};
use std::path::{Path, PathBuf};
use std::fs::File;


// trait can not be used as Box<dyn> at the moment because of generics etc
pub trait DataStore
{
    fn get<T: DeserializeOwned>(&self, key: &str) -> Result<T, anyhow::Error>; 

    fn set<T: Serialize>(&self, key: &str, v: &T) -> Result<(), anyhow::Error>;
}

///
/// Simple file based store to save/get data from folder
/// Use to provide a persistent store for modules
/// Uses cbor as file format
/// 
pub struct FileStore
{
    folder: PathBuf,
}

impl FileStore
{
    pub fn new(folder: &Path) -> Result<Self, anyhow::Error>
    {
        if !folder.is_dir() {
            return Err(anyhow::anyhow!("The path '{}' should be existing directory", folder.display()));
        }
        let store = FileStore{
            folder: folder.to_owned()
        };
        Ok(store)
    }

    // return the value or initialize via lambda
    pub fn get_or_init<T: DeserializeOwned+Serialize, F>(&self, key: &str, init: F) -> Result<T, anyhow::Error>
        where F: FnOnce() -> T
    {
        let config = {
            let p = match self.get(key)
            {
                Ok(p) => p,
                Err(_) => {
                    let p = init();
                    self.set(key, &p)?;
                    p
                }
            };
            p
        };
        Ok(config)
    }
}

impl DataStore for FileStore
{

    fn get<T: DeserializeOwned>(&self, key: &str) -> Result<T, anyhow::Error>
    {
        let mut p = self.folder.clone();
        p.push(key);
        p.set_extension("cbor");
        //TODO Check fi file exists before?
        let file = File::open(&p)?;
        let value: T = serde_cbor::from_reader(&file)?;
        Ok(value)
    }

    fn set<T: Serialize>(&self, key: &str, v: &T) -> Result<(), anyhow::Error>
    {
        let mut p = self.folder.clone();
        p.push(key);
        p.set_extension("cbor");
        //log::info!("write to file: {}", p.display());

        let mut file = File::create(&p)?;
        serde_cbor::to_writer(&mut file, &v)?;
        Ok(())
    }
}
