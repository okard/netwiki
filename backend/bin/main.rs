
use std::sync::Arc;
use std::path::PathBuf;

use log::{info};

use env_logger::Builder;
use argh::FromArgs;

#[derive(FromArgs)]
/// Netwiki Backend
struct CliOptions 
{
    #[argh(subcommand)]
    command: CliCommand,
}

#[derive(FromArgs)]
#[argh(subcommand)]
enum CliCommand 
{
    Init(InitCommand),
    Run(RunCommand),
}

#[derive(FromArgs, PartialEq, Debug)]
#[argh(subcommand, name = "init")]
/// Init 
struct InitCommand 
{
    /// datafolder
    #[argh(option)]
    path: String,

    //features?
}

#[derive(FromArgs, PartialEq, Debug)]
#[argh(subcommand, name = "run")]
/// Run
struct RunCommand 
{
    /// datafolder
    #[argh(option)]
    path: String,

    //modules? enable-p2p and so on
}


//config traits implement for toml struct stub


fn main()
{
    //setup log environment logger
    let mut builder = Builder::from_default_env();
    builder.filter_level(log::LevelFilter::Info);
    builder.init();


    let cli_options: CliOptions = argh::from_env();

    let result = match cli_options.command
    {
        CliCommand::Init(ref args) => init(args),
        CliCommand::Run(ref args) => run(args),
    };
    result.unwrap();
    // argument parsing
        // init --path
        // run --path

    // config (toml file?)

    //TODO initial setup admin identity when not exists?


}

fn init(args: &InitCommand) -> Result<(), anyhow::Error>
{
    info!("init netwiki instance in '{}'", args.path);
    // create folder etc
    // Create Backend structre and add additional module configs to the (config)store
    // add module configs
        // p2p
        // web api
        // initialize root identity

    // print username + password for the initial admin user

    Ok(())
}

fn run(args: &RunCommand) -> Result<(), anyhow::Error>
{
    info!("start netwiki backend");
    use netwiki::{BackendConfig, Backend};

    // backend
    let full_data_path = PathBuf::from(&args.path).canonicalize()?;
    let backend_config = BackendConfig {
        data_folder: full_data_path
    };

    let backend = Backend::new(backend_config)?; 
    let backend = Arc::new(backend);

    //which modules to start?
    
    // p2p module
    let _p = netwiki::run_module_p2p(backend.clone())?;
    
    // web api module
    netwiki::run_module_webapi(backend.clone())
        .map_err(|err| anyhow::anyhow!("error: {}", err.to_string()))?; //todo run with options

    Ok(())
}