import { h, Component } from './preact.js';

import { Backend } from "./backend.js";
import { Router } from "./routing.js";

import { HeaderNavBar } from './wiki/HeaderNavBar.js';
import { SideNavBar } from './wiki/SideNavBar.js';
import { Repositories } from './wiki/repositories.js';
import { Content } from './wiki/Content.js';
import { Constraint } from './constraint.js'

export interface WikiProps
{
    router: Router,
    backend: Backend,
    onLogout: () => void;
}

enum WikiView
{
    Page,
    Search,
    Repositories,
    //Image editor
}

export class Wiki extends Component<WikiProps>
{
  constructor(props: WikiProps)
  {
      super(props);
  }

  componentDidMount()
  {
      //register sub routes
  }

  componentWillUnmount()
  {
      //deregister sub routes
  }

  handleLogout = () =>
  {
      this.props.onLogout();
  }

  render(props: WikiProps)
  {
      // return right main content

      //header_nav
      //side_nav
      //content
    return <div>
        <HeaderNavBar router={props.router} user={props.backend.get_user()} onLogout={this.handleLogout} />

            <div class="columns">
                <Constraint path={/\/wiki/g} >

                    <SideNavBar />
                    <Content />
                </Constraint>
                
                <Repositories backend={props.backend}/>
            </div>
    </div>
  }
}
