
import { h, Component, ComponentChild} from './preact.js';

export interface ConstraintProps
{
    path : RegExp
}

export class Constraint extends Component<ConstraintProps>
{
    render(props: any)
    {
        //render only matched childs?
        return <div>{this.props.children}</div>;
    }
}
