
export interface IRouteSender
{
    onRoute: (rc : IRouteCommand) => void;
}

export interface IRouteCommand
{
    //route
    //e.g. /repostories/asdada/
    // /page/blabla
}




export class Router
{
    private routeSegments: RouteSegment[];

    constructor()
    {
        window.addEventListener("hashchange", this.onHashChange, false);
        window.addEventListener("popstate", this.onPopState, false);

        //TODO parse location and replace state
        var path = Router.get_path();
        console.log("path: %s from router ctor", path);
        history.replaceState({ path: path }, `NetWiki - ${path}`, path)
    }
    //root routesegment


    onPopState = (event: PopStateEvent) =>
    {
        //event.preventDefault();
        // Notification?
        console.log("pop state %o (%s)", event.state, location.href);
    }

    //parse location

    onHashChange = (event: Event) =>
    {
        event.preventDefault();

        let path = Router.get_path();
        console.log("path: %s from hash change", path);
        let title = `NetWiki - ${path}`;
        document.title = title
        history.replaceState({ path: path }, title, path);

        //parse
        //push history
    }

    public go_to(path: string)
    {
        //TODO go to
        location.hash = `#${path}`;
    }

    public handle_nav = (event: Event) =>
    {
        if (!(event.target instanceof HTMLAnchorElement))
        {
            return;
        }
        let link = event.target as HTMLAnchorElement;
        if(link.href.startsWith("#"))
        {
            event.preventDefault();
            location.hash = link.href;
        }
    }


    private static get_path() : string
    {
        var path = location.hash.substr(1);
        if(!path)
        {
            path = "/";
        }
        return path;
    }



    // #/abc/abc -> /abc/abc
    //


    /*
    var stateObj = { foo: "bar" };
    history.pushState(stateObj, "page 2", "bar.html");

    history.replaceState(stateObj, "page 3", "bar2.html");

    var currentState = history.state;

    window.addEventListener("hashchange", funcRef, false);

    read if (location.hash === "#irgendeinCoolesFeature")
     */

    //history

}


interface Dictionary<T> {
    [Key: string]: T;
}

type RouteArgs = Dictionary<any>;


// extract route parameter (use route
export class RouteSegment
{
    // save state
    private router : Router;
    /*
    // idea:
    // start with /repo/<rid>/page/<pid>
    let [args, subroute] = RouteSegment.derive("/repo/<rid>")
    let pid = args["rid"]
    let [args, subroute] = subroute.derive("/page/<pid>")

    save in stack for updates
    */


    public push(tpl: string) : [RouteArgs, RouteSegment]
    {
        return null;
    }

    public pop()
    {

    }

    // notify()

    // register for change event // auto fire?
}
