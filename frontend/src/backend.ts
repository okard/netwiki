
import * as model from "./model.js";
//token handler?

export interface BackendState
{
    url: string;
    token: model.TokenResponse;
}

export class Backend
{
    private url : URL;
    private currentToken : model.TokenResponse;

    private constructor(url : string)
    {
        this.url = new URL(url);
    }

    public static async InitializeFromLogin(url: string, user: string, password: string) : Promise<Backend>
    {
        var backend = new Backend(url);
        await backend.get_token(user, password);
        return backend;
    }

    public static InitializeFromState(state: BackendState) : Backend
    {
        var backend = new Backend(state.url);
        backend.currentToken = state.token;
        return backend;
    }

    private static get_json_payload(method: string, obj?: any) : RequestInit
    {
        // 'Authorization': ,
        let options : RequestInit =
        {
            method: method,
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            },
            mode: "cors",
            cache: "no-cache"
        };
        if(obj)
        {
            options.body = JSON.stringify(obj);
        }
        return options;
    }

    private set_auth(headers: HeadersInit) : void
    {
        /*if(<Headers>headers)
        {
            (<Headers>headers).set("Authorization", `Bearer ${this.currentToken.access_token}`);
            return;
        }*/
        if(<{[key: string]: string}>headers)
        {
            (<{[key: string]: string}>headers)["Authorization"] = `Bearer ${this.currentToken.access_token}`;
            return;
        }
        throw "Invalid header type";
    }


    private async get_token(user: string, password: string) : Promise<void>
    {
        let token_request = new model.TokenRequest();
        token_request.user = user;
        token_request.password = password;

        const setToken = (t : model.TokenResponse) => this.currentToken = t;
        const url = new URL('wiki/token', this.url);
        const payload = Backend.get_json_payload('POST', token_request);
        const response = await fetch(url.href, payload);
        if (!response.ok) {
            throw Error(response.statusText);
        }
        const myJson = await response.json();
        const token = (myJson as model.TokenResponse);
        setToken(token);
        console.log("token: %o", token);
    }

    public get_state() : BackendState
    {
        return {
            url: this.url.href,
            token: this.currentToken
        } as BackendState;
    }

    //TODO token refresh

    public get_user() : string
    {
        let token_infos = this.currentToken.access_token.split(".", 1)[0];
        let decode = JSON.parse(window.atob(token_infos)) as any;
        return decode.payload.user;
    }

    public get_backenduri(): string
    {
        return this.url.href;
    }

    public async get_repositories() : Promise<Array<model.Repository>>
    {
        console.log("backend: get_repositories");
        const url = new URL('wiki/repositories', this.url);
        let payload : RequestInit = {
            method: "GET",
            mode: "cors",
            cache: "no-cache",
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            }
        };
        this.set_auth(payload.headers);
        const response = await fetch(url.href, payload);
        if (!response.ok) {
            throw Error(response.statusText);
        }
        const jsonobj : Array<model.Repository> = await response.json();
        return jsonobj;
    }

    public async create_repository() : Promise<string>
    {
        console.log("backend: create_repository");
        const url = new URL('wiki/repositories', this.url);
        let payload : RequestInit = {
            method: "POST",
            mode: "cors",
            cache: "no-cache",
            headers: {}
        };
        this.set_auth(payload.headers);
        const response = await fetch(url.href, payload);
        if (!response.ok) {
            throw Error(response.statusText);
        }
        return response.headers.get("Location");
    }


    // Identity
    //



    //page


}
