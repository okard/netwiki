import { h, Component } from './preact.js';
import { Backend } from "./backend.js";

export interface LoginProps
{
    onLogin: (backend: Backend) => void;
}

interface LoginState
{
    //state
    //request_running, error,
    errorMessage: string;
}

export class Login extends Component<LoginProps, LoginState>
{
    constructor(props: LoginProps)
    {
        super(props);
        this.state = {  } as LoginState;
    }

    componentDidMount()
    {
    }

    componentWillUnmount()
    {
    }

    handleSubmit = async (event: Event) =>
    {
        event.preventDefault();
        const backend_url = (event.target as any)['backend'].value;
        const user = (event.target as any)['user'].value;
        const password = (event.target as any)['password'].value;
        (event.target as any)['password'].value = null;

        console.log("handle login for %s", user);

        try
        {
            this.setState({errorMessage: null})
            var backend = await Backend.InitializeFromLogin(backend_url, user, password);
            this.props.onLogin(backend);
        }
        catch(ex)
        {
            this.setState({errorMessage: ex.toString()})
        }

    }

    render(_: LoginProps, state: LoginState)
    {
        return <form class="form login-center box" onSubmit={this.handleSubmit}>
            <div class="hero is-primary my-1">
              <div class="hero-body">
                  <h1 class="title">NetWiki - Login</h1>
              </div>
            </div>

            <div class="field is-horizontal">
                <div class="field-label is-normal">
                    <label class="label" for="backend">Backend</label>
                </div>
                <div class="field-body">
                    <input class="input" type="url" name="backend" placeholder="Backend URI" value="http://127.0.0.1:8085/" required/>
                </div>
            </div>

            {/* Identity: */}

            <div class="field is-horizontal">
                <div class="field-label is-normal">
                    <label class="label" for="username">User:</label>
                </div>
                <div class="field-body">
                    <input class="input" type="text" name="user" placeholder="User" required/>
                </div>
            </div>

            <div class="field is-horizontal">
                <div class="field-label is-normal">
                    <label class="label" for="password">Password:</label>
                </div>
                <div class="field-body">
                    <input class="input" type="password" name="password" placeholder="Password" required/>
                </div>
            </div>

            <div class="field is-horizontal">
              <div class="field-label" />
              <div class="field-body">
                <div class="field">
                    {state.errorMessage !== null &&
                         <div class="field"><p class="help is-danger">{state.errorMessage}</p></div>
                    }
                  <div class="control">
                    <input class="button is-success" type="submit" value="Login" />
                  </div>
                </div>
              </div>
            </div>

        </form>;
    }
}
