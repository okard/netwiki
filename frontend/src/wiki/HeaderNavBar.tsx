
import { h, Component } from '../preact.js';
import { Router } from "../routing.js";

export interface HeaderBarProps
{
    router: Router,
    user: string,
    onLogout: () => void;
}

export class HeaderNavBar extends Component<HeaderBarProps>
{
  constructor(props: HeaderBarProps)
  {
    super(props);
  }

  componentDidMount()
  {
  }

  showIdentity = (event: Event) =>
  {
      event.preventDefault();
      this.props.router.go_to("/identity");
  }

  handleSearch = (event: KeyboardEvent) =>
  {
      if (event.key === "Enter")
      {
          event.preventDefault();

          let input = event.target as HTMLInputElement;
          console.log("do search %s", input.value);
          input.value = "";
      }
  }

  handleLogout = (event: Event) =>
  {
      event.preventDefault();
      this.props.onLogout();
  }

  render(props: HeaderBarProps)
  {
    return <header class="header">

        <nav class="level">
            <div class="level-left">
                <b class="level-item">NetWiki</b>
                <a class="level-item" href="#/repositories">Repositories</a>
                <span class="level-item">Current Repo: </span>
            </div>

            <div class="level-right mx-1">
                <input class="level-item input is-small" type="search" placeholder="search" onKeyPress={this.handleSearch} />
                <a class="level-item" href="#" onClick={this.showIdentity}>Identity: {props.user}</a>
                <a class="level-item button is-light" href="#" onClick={this.handleLogout}>Logout</a>
            </div>
        </nav>
    </header>
  }
}
