
import { h, Component } from '../preact.js';

export class SideNavBar extends Component
{
  constructor()
  {
    super();
  }

  componentDidMount()
  {
  }

  render()
  {
    return <aside class="column is-one-quarter sidebar">
        <nav>
            {/* nav bar */}
            <ul>
                <li>Homepage</li>
            </ul>
        </nav>
    </aside>
  }
}
