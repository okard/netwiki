
import { h, Component } from '../preact.js';
import * as model from "../model.js";
import { Backend } from "../backend.js";

class RepostoriesProps
{
    backend: Backend
}

class RepostoriesState
{
    data: Array<model.Repository>
}

export class Repositories extends Component<RepostoriesProps, RepostoriesState>
{
  constructor()
  {
    super();
    this.state = { data: [] };
  }

  componentDidMount()
  {
      console.log("handleCreateRepository");
      this.props.backend.get_repositories()
       .then(repos => this.setState({ data: repos }));
  }

  handleCreateRepository = (event: Event) =>
  {
      console.log("handleCreateRepository");
      event.preventDefault();
      this.props.backend.create_repository()
        .then(() => this.props.backend.get_repositories())
        .then(repos => this.setState({ data: repos }));
  }

  render()
  {
    return<div>
        <a href="#" onClick={this.handleCreateRepository}>Create Repositories</a>

        <ul>
            {this.state.data.map(el => (
            <li>
              <a href="#">{el.repository_id}</a>
            </li>
          ))}
        </ul>
    </div>
  }
}
