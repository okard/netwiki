import { h, Component } from "./preact.js";
import { Backend, BackendState } from "./backend.js";
import { Router } from "./routing.js";
import { Login } from "./login.js";
import { Wiki } from "./wiki.js";

enum AppView {
    Login,
    Wiki
}

interface AppState
{
    current_content: AppView,
    backend: Backend
}

export class App extends Component<{}, AppState>
{
    private readonly router: Router;
    private storage: ApplicationStorage = new ApplicationStorage();

  constructor()
  {
    super();
    this.router = new Router();

    //state manager?
    // check if login is there? sessionStorage
    if(this.storage.get_backend_state()) {
        //TODO on error go to login page
        var backend = Backend.InitializeFromState(this.storage.get_backend_state());
        this.state = { current_content: AppView.Wiki, backend: backend } as AppState;
    } else {
        this.router.go_to("/login");
        this.state = { current_content: AppView.Login } as AppState;
    }
  }

  componentDidMount()
  {
  }


  handleLogin = (backend: Backend) =>
  {
      this.storage.set_backend_state(backend.get_state())
      this.router.go_to('/startpage')
      this.setState({ current_content: AppView.Wiki, backend: backend });
  }

  handleLogout = () =>
  {
       this.storage.delete_backend_state();
       this.setState({ current_content: AppView.Login });
  }

  render(_props: {}, state: AppState)
  {
      // return right main content
      if(state.current_content == AppView.Login)
      {
          return <Login onLogin={this.handleLogin} />;
      }
      return <Wiki router={this.router} backend={state.backend} onLogout={this.handleLogout} />
  }
}


class ApplicationStorage
{
    get_backend_state() : BackendState
    {
        var data = window.localStorage.getItem('backend_state');
        var state : BackendState = JSON.parse(data);
        return state;
    }

    set_backend_state(state: BackendState) : void
    {
        window.localStorage.setItem('backend_state', JSON.stringify(state));
    }

    delete_backend_state():void
    {
        window.localStorage.removeItem('backend_state');
    }

}
