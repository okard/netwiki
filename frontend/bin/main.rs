
use actix_web::*;
use actix_web::dev::{ServiceRequest, ServiceResponse};
use actix_files as fs;

async fn default_service_handler(req: ServiceRequest) -> Result<ServiceResponse>
{
    let f = default_handler()?;
    let (req, _payload) = req.into_parts();
    let f = f.respond_to(&req).await?;
    Ok(ServiceResponse::new(req, f))
}

fn main() -> Result<(), Error>
{
    HttpServer::new(|| {
            let static_file_handler = fs::Files::new("/", ".")
                .default_handler(default_service_handler)
                .index_file("index.html");
            let app = App::new()
               .service(static_file_handler);
            app
        })
       .workers(4)
       .backlog(20)
       .bind("127.0.0.1:8000")?
       .bind("[::1]:8000")?
       .run();

    Ok(())
}

fn default_handler() -> Result<fs::NamedFile> {
    use fs::NamedFile;
    let file = NamedFile::open("index.html")?;
    Ok(file)
}
